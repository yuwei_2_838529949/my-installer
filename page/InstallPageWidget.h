#pragma once

#include <QWidget>
#include "ui_InstallPageWidget.h"
#include <QTimer>
#include <QPropertyAnimation>

QT_BEGIN_NAMESPACE
namespace Ui { class InstallPageWidgetClass; };
QT_END_NAMESPACE

class InstallPageWidget : public QWidget
{
	Q_OBJECT

public:
	InstallPageWidget(QWidget* parent = nullptr);
	~InstallPageWidget();
	void start();
	void stop();
protected:
	void showEvent(QShowEvent* e)override;
private:
	void init();
private slots:
	void onTimeOut();
	void onInstallFinished(bool ret);
	void onInstallProgress(int progress);
private:
	Ui::InstallPageWidgetClass* ui;
	QTimer m_timer;
	QVector<QImage> m_images;
	int m_curIndex = 0;
};
