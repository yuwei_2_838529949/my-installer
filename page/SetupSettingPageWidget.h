#pragma once

#include <QWidget>
#include "ui_SetupSettingPageWidget.h"
#include <QTimer>

QT_BEGIN_NAMESPACE
namespace Ui { class SetupSettingPageWidgetClass; };
QT_END_NAMESPACE

class SetupSettingPageWidget : public QWidget
{
	Q_OBJECT

public:
	SetupSettingPageWidget(QWidget* parent = nullptr);
	~SetupSettingPageWidget();
	QString getInstallPath()const;
	static SetupSettingPageWidget* instance();
private slots:
	void on_browseBtn_clicked();
	void on_backBtn_clicked();
	void on_nextBtn_clicked();
private:
	Ui::SetupSettingPageWidgetClass* ui;
	static SetupSettingPageWidget* m_instance;
	QTimer m_timer;
};
