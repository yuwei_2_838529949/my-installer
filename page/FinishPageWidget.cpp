#include "FinishPageWidget.h"
#include "Installer.h"

FinishPageWidget::FinishPageWidget(QWidget* parent)
	: QWidget(parent)
	, ui(new Ui::FinishPageWidgetClass())
{
	ui->setupUi(this);
	setAttribute(Qt::WA_StyledBackground);
	ui->label->setPixmap(QPixmap(":/image/win_fotor_Carousel_3.jpg"));
}

FinishPageWidget::~FinishPageWidget()
{
	delete ui;
}

void FinishPageWidget::on_pushButton_clicked()
{
	if (ui->startAppCbx->isChecked()) {
		Installer::instance()->startApp();
	}
	if (ui->shortCutCbx->isChecked()) {
		Installer::instance()->createShortcut();
	}
	qApp->exit(0);
}
