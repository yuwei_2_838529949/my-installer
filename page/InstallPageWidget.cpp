#include "InstallPageWidget.h"
#include <Installer.h>
#include <Widget.h>
#include "SetupSettingPageWidget.h"

InstallPageWidget::InstallPageWidget(QWidget* parent)
	: QWidget(parent)
	, ui(new Ui::InstallPageWidgetClass())
{
	ui->setupUi(this);
	setAttribute(Qt::WA_StyledBackground);
	m_timer.setInterval(3000);
	connect(&m_timer, &QTimer::timeout, this, &InstallPageWidget::onTimeOut);
	connect(Installer::instance(), &Installer::installFinished, this, &InstallPageWidget::onInstallFinished);
	connect(Installer::instance(), &Installer::installProgress, this, &InstallPageWidget::onInstallProgress);
	init();
}

InstallPageWidget::~InstallPageWidget()
{
	delete ui;
}

void InstallPageWidget::start()
{
	m_timer.start();
}

void InstallPageWidget::stop()
{
	m_timer.stop();
}

void InstallPageWidget::showEvent(QShowEvent* e)
{
	start();
	QString installPath = SetupSettingPageWidget::instance()->getInstallPath();
	Installer::instance()->startInstall(installPath);
}

void InstallPageWidget::init()
{
	m_images.append(QImage(":/image/win_fotor_Carousel_1.jpg"));
	m_images.append(QImage(":/image/win_fotor_Carousel_2.jpg"));
	m_images.append(QImage(":/image/win_fotor_Carousel_3.jpg"));
	m_images.append(QImage(":/image/win_fotor_Carousel_4.jpg"));
	m_images.append(QImage(":/image/win_fotor_Carousel_5.jpg"));
	QImage image = m_images.at(0);
	ui->label->setPixmap(QPixmap::fromImage(image));
}

void InstallPageWidget::onInstallFinished(bool ret)
{
	Widget::instance()->goNext();
}

void InstallPageWidget::onInstallProgress(int progress)
{
	ui->progressBar->setValue(progress);
}

void InstallPageWidget::onTimeOut()
{
	m_curIndex++;
	if (m_curIndex > m_images.size() - 1)
		m_curIndex = 0;
	QImage image = m_images.at(m_curIndex);
	ui->label->setPixmap(QPixmap::fromImage(image));
}