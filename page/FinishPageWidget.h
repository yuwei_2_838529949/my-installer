#pragma once

#include <QWidget>
#include "ui_FinishPageWidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class FinishPageWidgetClass; };
QT_END_NAMESPACE

class FinishPageWidget : public QWidget
{
	Q_OBJECT

public:
	FinishPageWidget(QWidget* parent = nullptr);
	~FinishPageWidget();
private slots:
	void on_pushButton_clicked();
private:
	Ui::FinishPageWidgetClass* ui;
};
