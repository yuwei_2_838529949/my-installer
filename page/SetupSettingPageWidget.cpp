#include "SetupSettingPageWidget.h"
#include "../Widget.h"
#include <Common.h>
#include <QFileDialog>
#include <QStandardPaths>



SetupSettingPageWidget* SetupSettingPageWidget::m_instance = nullptr;

SetupSettingPageWidget::SetupSettingPageWidget(QWidget* parent)
	: QWidget(parent)
	, ui(new Ui::SetupSettingPageWidgetClass())
{
	ui->setupUi(this);
	ui->label_2->setPixmap(QPixmap(":/image/win_fotor_home.jpg"));
	setAttribute(Qt::WA_StyledBackground);
	m_instance = this;
	// 设置默认安装目录
	QString userHomePath = QStandardPaths::writableLocation(QStandardPaths::HomeLocation) + "/" + Commom::InstallFolderName;
	ui->lineEdit->setText(userHomePath);
}

SetupSettingPageWidget::~SetupSettingPageWidget()
{
	delete ui;
}

QString SetupSettingPageWidget::getInstallPath() const
{
	return ui->lineEdit->text();
}

SetupSettingPageWidget* SetupSettingPageWidget::instance()
{
	return m_instance;
}

void SetupSettingPageWidget::on_browseBtn_clicked()
{
	QString homePath = QFileDialog::getExistingDirectory(nullptr, "选择目录", QDir::homePath());
	homePath += "/" + Commom::InstallFolderName;
	ui->lineEdit->setText(homePath);
}

void SetupSettingPageWidget::on_backBtn_clicked()
{
	Widget::instance()->goBack();
}

void SetupSettingPageWidget::on_nextBtn_clicked()
{
	// 检查目录是否存在
	QString installPath = getInstallPath();
	QDir dir;
	dir.mkdir(installPath);
	Widget::instance()->goNext();
}