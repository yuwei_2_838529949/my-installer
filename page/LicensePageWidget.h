#pragma once

#include <QWidget>
#include "ui_LicensePageWidget.h"

QT_BEGIN_NAMESPACE
namespace Ui { class LicensePageWidgetClass; };
QT_END_NAMESPACE

class LicensePageWidget : public QWidget
{
	Q_OBJECT

public:
	LicensePageWidget(QWidget* parent = nullptr);
	~LicensePageWidget();
private slots:
	void on_nextBtn_clicked();
private:
	Ui::LicensePageWidgetClass* ui;
};
