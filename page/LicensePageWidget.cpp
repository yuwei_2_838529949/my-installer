#include "LicensePageWidget.h"
#include "../Widget.h"
#include <UIFunctionsDefine.h>
#include <QFile>

LicensePageWidget::LicensePageWidget(QWidget* parent)
	: QWidget(parent)
	, ui(new Ui::LicensePageWidgetClass())
{
	ui->setupUi(this);
	setAttribute(Qt::WA_StyledBackground);
	//UIFuntions::loadStyleSheetFromFile(this, "/qss/licensepage.qss");
	connect(ui->checkBox, &QCheckBox::stateChanged, [this] {
		if (ui->checkBox->isChecked()) {
			ui->checkBox->setStyleSheet("color:white");
		}
		});
	QFile file(":/license/license.htm");
	file.open(QIODevice::ReadOnly);
	QString html = file.readAll();
	ui->textBrowser->setHtml(html);
}

LicensePageWidget::~LicensePageWidget()
{
	delete ui;
}

void LicensePageWidget::on_nextBtn_clicked()
{
	if (ui->checkBox->isChecked()) {
		// 转到下一页
		Widget::instance()->goNext();
	}
	else {
		// 置红
		ui->checkBox->setStyleSheet("color:red");
	}
}