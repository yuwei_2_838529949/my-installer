#include "Widget.h"
#include <QtWidgets/QApplication>
#include <QMessageBox>
#include <Installer.h>

int main(int argc, char* argv[])
{
	QApplication a(argc, argv);
	int ret = 0;
	Installer::instance()->init();
	if (argc > 1) {
		const char* arg = argv[1];
		if (0 == strcmp(arg, "--mode=install")) {
			// 安装
			Widget w;
			w.show();
			ret = a.exec();
		}
		else if (0 == strcmp(arg, "--mode=uninstall")) {
			// 卸载
			QMessageBox::StandardButton btn = QMessageBox::information(nullptr, "卸载", "你想卸载程序吗?", QMessageBox::Yes | QMessageBox::No);
			if (btn == QMessageBox::No) {
				return 0;
			}
			Installer::instance()->startUnInstall();
			QMessageBox::information(nullptr, "卸载程序", "卸载完成");
		}
	}
	Installer::instance()->uninit();
	return ret;
}
