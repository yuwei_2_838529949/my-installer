﻿icon ".\32bpp.ico"  #设置图标
OutFile "setup64.exe"  #设置生成程序
SilentInstall silent  #静默安装不出现任何NSIS自带界面

!define MAIN_PROGRAM_NAME "setup64.exe"


Function .onInit  #NSIS程序安装准备工作
InitPluginsDir  #创建临时目录（内存）用于保存一些必要的库
SetOutPath $PLUGINSDIR  #将文件保存在临时目录中
  File /r "x64\debug\*"
FunctionEnd


Function .onInstSuccess  #安装成功后调用
call fun
Functionend


Function fun
   ExecWait "$PLUGINSDIR\MyInstaller.exe --mode=install"  #调用QT安装程序，等待其运行完后运行下一条语句
   RMDir $PLUGINSDIR                 #释放临时目录（内存）
FunctionEnd

Section "setup64"

	WriteRegStr HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers" "$INSTDIR\${MAIN_PROGRAM_NAME}" "RUNASADMIN"
 
SectionEnd


