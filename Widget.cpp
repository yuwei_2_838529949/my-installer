#include "Widget.h"

Widget* Widget::m_instace = nullptr;

Widget::Widget(QWidget* parent)
	: UseStyleSheetWidgetBase(parent)
	, ui(new Ui::WidgetClass())
{
	ui->setupUi(this);
	setCanMoveWithMouse(true);
	setAttribute(Qt::WA_TranslucentBackground, true);
	ui->stackedWidget->setCurrentIndex(0);
	m_instace = this;
}

Widget::~Widget()
{
	delete ui;
}

void Widget::goNext()
{
	int index = ui->stackedWidget->currentIndex();
	ui->stackedWidget->setCurrentIndex(++index);
}

void Widget::goBack()
{
	int index = ui->stackedWidget->currentIndex();
	ui->stackedWidget->setCurrentIndex(--index);

}

Widget* Widget::instance()
{
	return m_instace;
}

void Widget::resizeEvent(QResizeEvent* ev)
{
	setCanDragArea(ui->titleWgt->rect());
	UseStyleSheetWidgetBase::resizeEvent(ev);
}
