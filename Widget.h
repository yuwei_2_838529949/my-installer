#pragma once
#include "ui_Widget.h"
#include "UseStyleSheetWidgetBase.h"

QT_BEGIN_NAMESPACE
namespace Ui { class WidgetClass; };
QT_END_NAMESPACE

class Widget : public UseStyleSheetWidgetBase
{
	Q_OBJECT

public:
	Widget(QWidget* parent = nullptr);
	~Widget();
	// 下一步
	void goNext();
	// 上一步
	void goBack();
	static Widget* instance();
protected:
	void resizeEvent(QResizeEvent* ev)override;
private:
	Ui::WidgetClass* ui;
	static Widget* m_instace;
};
