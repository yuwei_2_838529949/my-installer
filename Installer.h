#pragma once
#include <QThread>
#include <QJsonObject>

class Installer : public QObject
{
	Q_OBJECT
public:
signals:
	void installFinished(bool ok);
	void installProgress(int);
public:
	static Installer* instance();

	bool isAppRun();
	bool isAppExists(const QString& targetPath);
	bool terminateApp();
	bool isInstallerRun();

	QString installPath();

	void startInstall(const QString& targetPath);
	bool isInstalling();

	void startUnInstall();

	void createShortcut();
	void removeShortcut();
	void startApp();

	void init();

	void uninit();
private:
	explicit Installer(QObject* parent = nullptr);
	void writeInstallLocation(const QString& path);
	void writeUninstallString(const QString& path);
	void writeInstallerInfomation();
	void writeMachineID(const QString& machineID);
	void removeInstallRegkeys();
	void removeApplicationFiles();
	// 读取注册表中的安装包信息
	QString readInstallLocation() const;
	QString readUninstallString() const;
	QString readMachineID() const;
	bool installThread();
	QString getShortcutPath(const QString& shortcut);
private:
	QThread* m_installThread = nullptr;
	quint64 m_extractTotalSize = 0;
	QString m_removeBatFilePath;
	// 安装路径
	QString m_installPath;
};

