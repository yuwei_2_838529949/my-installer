#ifndef UseStyleSheetWidgetBase_h__
#define UseStyleSheetWidgetBase_h__

#include <QtWidgets/QStyleOption>
#include <QtGui/QPainter>
#include <QtWidgets/QWidget>
#include "UseStyleSheetBase.h"
#include "FormlessWidgetMoveBase.h"

class UseStyleSheetWidgetBase
	: public QWidget
	, public UseStyleSheetBase
	, public FormlessWidgetMoveBase
{
	Q_OBJECT
public:
	UseStyleSheetWidgetBase(QWidget* pParent = 0) :
		QWidget(pParent),
		m_PassEvent(1),
		m_sendLeaveEvent(true)
	{
		setWindowFlags(windowFlags() | Qt::FramelessWindowHint);
	}
	void setPassEvent(bool v) { m_PassEvent = v; }

protected:
	void paintEvent(QPaintEvent* event)
	{

		QPainter p(this);
		UseStyleSheetBase::drawWithStyleSheet(p, this);

		if (!backgroundImage().isNull())
		{
			p.drawPixmap(rect(), backgroundImage());
		}

		QWidget::paintEvent(event);
	}

	void mousePressEvent(QMouseEvent* event)
	{
		FormlessWidgetMoveBase::onMousePressed(this, event);
		if (m_PassEvent)
		{
			QWidget::mousePressEvent(event);
		}
	}
	void mouseMoveEvent(QMouseEvent* event)
	{
		FormlessWidgetMoveBase::onMouseMoving(this, event);
		QWidget::mouseMoveEvent(event);
	}
	void mouseReleaseEvent(QMouseEvent* event)
	{
		FormlessWidgetMoveBase::onMouseRelease(this, event);
		QWidget::mouseReleaseEvent(event);
	}
	void setCanDragArea(const QRect& rect)
	{
		m_rect = rect;
	}

	virtual QPainterPath canDragArea() const
	{
		QPainterPath path;
		path.addRect(m_rect);
		return path;
	}
private:
	bool m_PassEvent;
	bool m_sendLeaveEvent;
	QRect m_rect;
};
#endif // UseStyleSheetWidgetBase_h__
