#ifndef UIFUNCTIONSDEFINE_H
#define UIFUNCTIONSDEFINE_H

#include <QtWidgets>
#include <QtCore>

namespace UIFuntions
{
	//加载StyleSheet文件
	QString readStyleSheetFile(const QString& strFilePath);
	QString readStyleSheetFile(const QStringList& listFilePath);

	//从文件加载StyleSheet并设置
	bool loadStyleSheetFromFile(QWidget* pWidget, const QString& strFilePath);
	bool loadStyleSheetFromFile(QWidget* pWidget, const QStringList& listFilePath);
	bool loadStyleSheetFromFile(QApplication* pApp, const QString& strFilePath);
	bool loadStyleSheetFromFile(QApplication* pApp, const QStringList& listFilePath);
}

#endif // UIFUNCTIONSDEFINE_H
