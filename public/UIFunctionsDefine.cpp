#include "UIFunctionsDefine.h"
#include <QtCore>
#include <QtWidgets>
#include <QPointer>

bool UIFuntions::loadStyleSheetFromFile(QWidget* pWidget, const QString& strFilePath)
{
	if (!pWidget)
	{
		return false;
	}

	const QString strStyleSheet = readStyleSheetFile(strFilePath);

	if (!strStyleSheet.isEmpty())
	{
		pWidget->setStyleSheet(strStyleSheet);
	}

	return !strStyleSheet.isEmpty();
}
bool UIFuntions::loadStyleSheetFromFile(QWidget* pWidget, const QStringList& listFilePath)
{
	if (!pWidget)
	{
		return false;
	}

	const QString strStyleSheet = readStyleSheetFile(listFilePath);

	if (!strStyleSheet.isEmpty())
	{
		pWidget->setStyleSheet(strStyleSheet);
	}

	return !strStyleSheet.isEmpty();
}

bool UIFuntions::loadStyleSheetFromFile(QApplication* pApp, const QString& strFilePath)
{
	if (!pApp)
	{
		return false;
	}

	const QString strStyleSheet = readStyleSheetFile(strFilePath);

	if (!strStyleSheet.isEmpty())
	{
		pApp->setStyleSheet(strStyleSheet);
	}

	return !strStyleSheet.isEmpty();
}

bool UIFuntions::loadStyleSheetFromFile(QApplication* pApp, const QStringList& listFilePath)
{
	if (!pApp)
	{
		return false;
	}

	const QString strStyleSheet = readStyleSheetFile(listFilePath);

	if (!strStyleSheet.isEmpty())
	{
		pApp->setStyleSheet(strStyleSheet);
	}

	return !strStyleSheet.isEmpty();
}

QString UIFuntions::readStyleSheetFile(const QString& strFilePath)
{
	if (strFilePath.isEmpty())
	{
		return "";
	}

	QString strStyleSheet;
	QString strTempPath = strFilePath;
	strTempPath = ":" + strTempPath;
	QFile file(strTempPath);
	if (file.open(QFile::ReadOnly))
	{
		strStyleSheet = file.readAll();
		file.close();

		return strStyleSheet;
	}
	else
	{
		qWarning() << QString(" Failed to load QSS file! File path: %1").arg(strTempPath);

		return strStyleSheet;
	}
}

QString UIFuntions::readStyleSheetFile(const QStringList& listFilePath)
{
	if (listFilePath.isEmpty())
	{
		return "";
	}

	QStringList listTempPath = listFilePath;
	listTempPath.removeDuplicates();

	QString strStyleSheet;
	for (int nIndex = 0; nIndex < listTempPath.size(); nIndex++)
	{
		const QString strTemp = readStyleSheetFile(listTempPath[nIndex]);

		strStyleSheet += strTemp;
	}

	return strStyleSheet;
}
