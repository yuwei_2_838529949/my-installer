#ifndef UseStyleSheetBase_h__
#define UseStyleSheetBase_h__

#include <QtWidgets/QStyleOption>
#include <QtGui/QPainter>
#include <QtGui/QPixmap>

class UseStyleSheetBase
{
public:
	//背景图片
	const QPixmap& backgroundImage() const { return m_backgroundImage; }
	void setBackgroundImage(const QString& url) { m_backgroundImage.load(url); }

protected:
	void drawWithStyleSheet(QPainter& painter, QWidget* widget)
	{
		if (widget)
		{
			QStyleOption opt;
			opt.initFrom(widget);
			widget->style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, widget);
		}
	}

private:
	QPixmap m_backgroundImage;

};
#endif // UseStyleSheetBase_h__
