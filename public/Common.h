#pragma once
#include <QString>

namespace Commom
{
	// 安装的程序名称
	const static QString InstallAppName = "SocketTool.exe";
	// 安装程序的文件夹名称
	const static QString InstallFolderName = "SocketTool";
	const static QString UninstallRootKey = "HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall";
	// 卸载程序的注册表key
	const static QString UninstallKey = UninstallRootKey + "\\" + InstallFolderName;
	// 程序在卸载中显示的版本
	const static QString DisplayVersion = "4.7.0";
}


